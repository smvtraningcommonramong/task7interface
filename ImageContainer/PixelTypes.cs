﻿
namespace ImageContainer
{
    /// <summary>
    /// Pixel Types
    /// </summary>
    public enum PixelTypes
    {
        // Signed (2 char) | Depth (2 char) | Channels (2 char)
        U8 = 0x010100,
        U16 = 0x010200,
        U32 = 0x010400,
        U64 = 0x010800,
        I8 = 0x020100,
        I16 = 0x020200,
        I32 = 0x020400,
        I64 = 0x020800,
        F32 = 0x040400,
        F64 = 0x040800
    }
}
