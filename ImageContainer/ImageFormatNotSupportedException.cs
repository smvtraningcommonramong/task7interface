﻿using System;
using System.IO;
using System.Runtime.Serialization;

namespace ImageContainer
{
    /// <summary>
    /// Custom Exception Class for image format not supported
    /// </summary>
    public class ImageFormatNotSupportedException : Exception
    {
        private static readonly string DefaultMessage = "Image Format Not Supported.";

        public ImageFormatNotSupportedException() : base()
        {

        }

        public ImageFormatNotSupportedException(string message) : base(message)
        {

        }

        public ImageFormatNotSupportedException(string message, Exception innerException) : base(message, innerException)
        {

        }

        public ImageFormatNotSupportedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }

        /// <summary>
        /// Thrown when the image format doesn't are into ImageFileType.
        /// </summary>
        /// <param name="fileType">Image extension</param>
        public ImageFormatNotSupportedException(ImageFileTypes fileType) : base(DefaultMessage)
        {
            foreach (ImageFileTypes i in Enum.GetValues(typeof(ImageFileTypes)))
            {
                fileType = i;
            }
        }


    }
}
