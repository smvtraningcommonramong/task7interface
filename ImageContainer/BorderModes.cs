﻿
namespace ImageContainer
{
    /// <summary>
    /// List of border types 
    /// </summary>
    public enum BorderModes
    {
        /// <summary>
        /// Pixel values outside boundaries will be the constant value. CCC|abcdefgh|CCC, with some specified C
        /// </summary>
        Constant,
        /// <summary>
        /// Pixel values will be the ones in the boundaries. aaa|abcdefgh|hhh
        /// </summary>
        BorderValue,
        /// <summary>
        /// Pixel values will be the ones reflected inside the boundaries. dcb|abcdefgh|gfe
        /// </summary>
        Mirror,
        /// <summary>
        /// >An exception will be raised.
        /// </summary>
        ThrowError
    }
}
