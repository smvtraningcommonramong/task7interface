﻿
namespace ImageContainer
{
    /// <summary>
    /// Mode given to each pixel in the image
    /// </summary>
    public enum ImageModes
    {
        Original,
        GrayScale,
    }
}
