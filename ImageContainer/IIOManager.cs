﻿using System.Drawing;

namespace ImageContainer
{
    /// <summary>
    /// Interface that defines the signatures of the input/output image functions 
    /// </summary>
    public interface IIOManager
    {
        /// <summary>
        /// Reads a whole image from a file path.
        /// </summary>
        /// <remarks>It operates over the image object itself setting its properties and pixel data.</remarks>
        /// <param name="url">Path of the image file.</param>
        /// <param name="imageMode">Read image mode.</param>
        /// <exception cref="System.IO.FileNotFoundException">Thrown when a file does not exist in the specified path.</exception>
        /// <exception cref="ImageFormatNotSupportedException">Thrown when the image format is not supported.</exception>
        void ReadImage(string url, ImageModes imageMode = ImageModes.Original);

        /// <summary>
        /// Read a specific image region (with a Rectangle) from a file path.
        /// </summary>
        /// <remarks>It operates over the image object itself setting its properties and pixel data.</remarks>
        /// <param name="url">Path of the image file.</param>
        /// <param name="rectROI">Rectangle that represents the region of interest.</param>
        /// <param name="imageMode">Read image mode.</param>
        /// <exception cref="System.IO.FileNotFoundException">Thrown when a file does not exist in the specified path.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">Throw when the ROI is out of the matrix boundaries.</exception>
        /// <exception cref="ImageFormatNotSupportedException">Thrown when the image format is not supported.</exception>
        void ReadImage(string url, Rectangle rectROI, ImageModes imageMode = ImageModes.Original);

        /// <summary>
        /// Read a specific image region (with start and end coordinates) from a file path.
        /// </summary>
        /// <remarks>It operates over the image object itself setting its properties and pixel data.</remarks>
        /// <param name="url">Path of the image file.</param>
        /// <param name="x1">ROI start X coordinate.</param>
        /// <param name="y1">ROI start Y coordinate.</param>
        /// <param name="x2">ROI end X coordinate.</param>
        /// <param name="y2">ROI end Y coordinate.</param>
        /// <param name="imageMode">Read image mode.</param>
        /// <exception cref="System.IO.FileNotFoundException">Thrown when a file does not exist in the specified path.</exception>
        /// <exception cref="ImageFormatNotSupportedException">Thrown when the image format is not supported.</exception>
        void ReadImage(string url, int x1, int y1, int x2, int y2, ImageModes imageMode = ImageModes.Original);

        /// <summary>
        /// Read a specific image region (with a Rectangle) from a file path and handles pixels outside image.
        /// </summary>
        /// <remarks>
        /// <para>It operates over the image object itself setting its properties and pixel data.</para>
        /// <para>If the ROI is out of the matrix, pixel values will be taken according to the BorderMode.</para>
        /// </remarks>
        /// <param name="url">Path of the image file.</param>
        /// <param name="rectROI">Rectangle that represents the region of interest.</param>
        /// <param name="borderMode">Type of border if the Rectangle is out of the matrix.</param>
        /// <param name="constantValue">Value used when BorderMode.ConstantValue is selected.</param>
        /// <param name="imageMode">Read image mode.</param>
        /// See <see cref="BorderModes"/>
        /// <exception cref="System.IO.FileNotFoundException">Thrown when a file does not exist in the specified path.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">Throw when the ROI is out of the matrix boundaries and the BorderMode is set to ThrowError.</exception>
        /// <exception cref="System.ArgumentNullException">Thrown when the BorderMode.ConstantValue is selected but ConstantValue is 0.</exception>
        /// <exception cref="ImageFormatNotSupportedException">Thrown when the image format is not supported.</exception>
        void ReadImage(string url, Rectangle rectROI, BorderModes borderMode, double constantValue = 0, ImageModes imageMode = ImageModes.Original);

        /// <summary>
        /// Read a specific image region (with start and end coordinates) from a file path and handles pixels outside image.
        /// </summary>
        /// <remarks>It operates over the image object itself setting its properties and pixel data.
        /// <para>If the coordinates are out of the matrix, pixel values will be taken according to the BorderMode</para>
        /// </remarks>
        /// <param name="url">Path of the image file.</param>
        /// <param name="x1">ROI start X coordinate.</param>
        /// <param name="y1">ROI start Y coordinate.</param>
        /// <param name="x2">ROI end X coordinate.</param>
        /// <param name="y2">ROI end Y coordinate.</param>
        /// <param name="borderMode">Type of border if the Rectangle is out of the matrix.</param>
        /// <param name="constantValue">Value used when BorderMode.ConstantValue is selected.</param>
        /// <param name="imageMode">Read image mode.</param>
        /// See <see cref="BorderModes"/>
        /// <exception cref="System.IO.FileNotFoundException">Thrown when a file does not exist in the specified path.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">Throw when the coordinates are out of the matrix boundaries and the BorderMode is set to ThrowError.</exception>
        /// <exception cref="System.ArgumentNullException">Thrown when the BorderMode.ConstantValue is selected but ConstantValue is 0.</exception>
        /// <exception cref="ImageFormatNotSupportedException">Thrown when the image format is not supported.</exception>
        void ReadImage(string url, int x1, int y1, int x2, int y2, BorderModes borderMode, double constantValue = 0, ImageModes imageMode = ImageModes.Original);

        /// <summary>
        /// Save an image to file path.
        /// </summary>
        /// <remarks>It takes the data from the image object itself and uses and encoder to save the image to disk.</remarks>
        /// <param name="url">Path to save the image.</param>
        /// <param name="imageFile">Type image file.</param>
        /// See <see cref="ImageFileTypes"/>
        /// <exception cref="System.ArgumentNullException">Thrown when the image object or the url are null.</exception>
        /// <exception cref="System.Runtime.InteropServices.ExternalException">Throw when the image is saved in the wrong image format or in the same file from which it was created.</exception>
        /// <exception cref="ImageFormatNotSupportedException">Thrown when the image format is not supported.</exception>
        void SaveImage(string url, ImageFileTypes imageFile = ImageFileTypes.Bmp);
    }

}
