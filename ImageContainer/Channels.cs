﻿
namespace ImageContainer
{
    /// <summary>
    /// Channel identifier list 
    /// </summary>
    public enum Channels
    {
        Red = 1,
        Green = 2,
        Blue = 4,
        Alpha = 8,
        All = 255
    }
}
