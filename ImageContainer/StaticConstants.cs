﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageContainer
{

    /// <summary>
    /// Class that holds all the constant values
    /// </summary>
    public class StaticConstants
    {
        /// <value>Current max number of supported image channels or planes</value>
        public const int SupportedChannels = 5;
    }
}
