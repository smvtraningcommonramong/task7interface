﻿using System;
using System.Drawing;

namespace ImageContainer
{
    public interface IImageContainer : IIOManager
    {
        #region Image properties

        /// <summary>Number of pixels from base to top in the image.</summary>
        int Height { get; }

        /// <summary>Number of pixels from left to rigth in the image.</summary>
        int Width { get; }

        /// <summary>The amount of pixels in the image, Height*Width.</summary>
        long TotalPixels { get; }

        /// <summary>The number of image layers.</summary>
        int Channels { get; }

        /// <summary>Define the pixel type and number of layers. Eg. U8C5.</summary>
        ImageTypes Type { get; }

        /// <summary>Pixel type (signed|unsigned of 8,16,32,64 bits).</summary>
        PixelTypes PixelType { get; }

        /// <summary>The amount of pixels in the stride.</summary>
        long Steps { get; }

        /// <summary> Returns a string with the height, width, image type and steps of the image.</summary>
        string ToString();

        #endregion

        #region Image management functions

        /// <summary>
        /// Creates a copy of an image without allocate new data.
        /// </summary>
        /// <remarks>
        /// <para>Only creates a new header that points to existing data.</para>
        /// </remarks>
        /// <exception cref="ArgumentNullException">Thrown when the image is null or empty.</exception>
        /// <returns>An image object with a new header.</returns>
        IImageContainer CopyImageRef();

        /// <summary>
        /// Copy the complete structure as well as data of another image.
        /// </summary>
        /// <remarks>
        /// <para>Allocates new data for the new object.</para>
        /// </remarks>
        /// <exception cref="ArgumentNullException">Thrown when the image is null or empty.</exception>
        /// <returns>An image object with the same structure as well as data.</returns>
        IImageContainer CloneImage();

        /// <summary>
        /// Converts an image to grayscales
        /// </summary>
        /// <remarks>
        /// <para>Change thge Image Container object to GrayScale.</para>
        /// </remarks>
        /// <exception cref="ArgumentNullException">Thrown when the image is null or empty.</exception>
        /// <returns>An Image Container converted to GrayScale.</returns>
        IImageContainer ConvertToGrayScale();

        /// <summary>
        /// Create a new version of the image with a different width and/or height.
        /// </summary>
        /// <remarks>
        /// <para>New height and width  must be greater than zero.</para>
        /// </remarks>
        /// <param name="newHeight">New number of rows.</param>
        /// <param name="newWidth">New number of columns.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when newHeight or newWidth are not valid sizes (<= 0) for the operation.</exception>
        /// <returns>An image object with a different size from the original.</returns>
        IImageContainer ResizeImage(int newHeight, int newWidth, InterpolationModes interpolationModes= InterpolationModes.Bilinear);

        /// <summary>
        /// Create a new version of the image with a different Size.
        /// </summary>
        /// <remarks>
        /// <para>New size must be greater than zero for height and width.</para>
        /// </remarks>
        /// <param name="newSize">New image size.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when newSize are not a valid Size (<= 0) for the operation.</exception>
        /// <returns>An image object with a different size from the original.</returns>
        IImageContainer ResizeImage(Size newSize, InterpolationModes interpolationModes = InterpolationModes.Bilinear);

        /// <summary>
        /// Create a new version of the image with a different Size based of resize factor.
        /// </summary>
        /// <remarks>
        /// <para>Obtain a new image object with modified Size.</para>
        /// </remarks>
        /// <param name="factor">Scale factor</param>  
        /// <exception cref="ArgumentOutOfRangeException">Thrown when factor are not a valid resize factor (<= 0) for the operation.</exception>
        /// <returns>An image object with a different size from the original.</returns>
        IImageContainer ResizeImage(float factor, InterpolationModes interpolationModes = InterpolationModes.Bilinear);

        /// <summary>
        /// Sets the specified PixelVal to the whole image.
        /// </summary>
        /// <remarks>
        /// <para>Values in PixelVal will be set to each image layer if they match in number.</para>
        /// <para>If only one value in PixelVal, that value will be set to all layers. If any of these options it throws an exception</para>
        /// </remarks>
        /// <param name="value">Intensites from PixelVal object to set.</param>
        /// <exception cref="ArgumentException">Thrown when the pixel value does not match the image pixel type.</exception>
        void SetTo(PixelVal value);

        /// <summary>
        /// Sets the specified value in whole the image.
        /// </summary>
        /// <remarks>
        /// <para>Modify the pixel intensity in all image array.</para>
        /// <para>The operation can be implemented with diferent pixel types.</para>
        /// </remarks>
        /// <param name="value">Intensity pixel value to set.</param>
        /// <exception cref="ArgumentNullException">Thrown when the image is null or empty.</exception>
        void SetTo(byte value);
        void SetTo(ushort value);
        void SetTo(int value);
        void SetTo(float value);
        void SetTo(double value);

        /// <summary>
        /// Sets the specified value in the image array
        /// </summary>
        /// <remarks>
        /// <para>Values in array will be set to each image layer if they match in number.</para>
        /// <para>If only one value in array, that value will be set to all layers. If any of these options it throws an exception</para>
        /// </remarks>
        /// <param name="value">Intensity pixel array to set.</param>
        /// <exception cref="ArgumentException">Thrown when the number of values in array is not valid for the image type.</exception>
        void SetTo(byte[] value);
        void SetTo(ushort[] value);
        void SetTo(int[] value);
        void SetTo(float[] value);
        void SetTo(double[] value);

        /// <summary>
        /// Sets a new value for the pixel in the given coordinates
        /// </summary>
        /// <remarks>
        /// <para>Values in PixelVal will be set to each image layer if they match in number.</para>
        /// <para>If only one value in PixelVal, that value will be set to all layers. If any of these options it throws an exception</para>
        /// </remarks>
        /// <param name="row">Position with vertical axis in Image Container.</param>
        /// <param name="col">Position with horizontal axis in Image Container.</param>
        /// <param name="value">Intensity PixelVal object to set.</param>
        /// <exception cref="ArgumentException">Thrown when the number of values in PixelVal is not valid for the image type.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the X or Y coordinates are out of the image boundaries.</exception>
        void SetPixel(int row, int col, PixelVal value);

        /// <summary>
        /// Sets a new value for the pixel in the given coordinates
        /// </summary>
        /// <remarks>
        /// <para>Modify the pixel intensity in all image array.</para>
        /// <para>The operation can be implemented with diferent pixel types.</para>
        /// </remarks>
        /// <param name="row">Position with vertical axis in Image Container.</param>
        /// <param name="col">Position with horizontal axis in Image Container.</param>
        /// <param name="value">Intensity pixel value to set.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the X or Y coordinates are out of the image boundaries.</exception>
        /// /// <exception cref="OverflowException">The exception that is thrown when the value exceeds the limit of data type.</exception>
        void SetPixel(int row, int col, byte value);
        void SetPixel(int row, int col, ushort value);
        void SetPixel(int row, int col, int value);
        void SetPixel(int row, int col, float value);
        void SetPixel(int row, int col, double value);

        /// <summary>
        /// Sets a new value for the pixel in the given coordinates
        /// </summary>
        /// <remarks>
        /// <para>Values in array will be set to each image layer if they match in number.</para>
        /// <para>If only one value in array, that value will be set to all layers. If any of these options it throws an exception</para>
        /// </remarks>
        /// <param name="row">Position with vertical axis in Image Container.</param>
        /// <param name="col">Position with horizontal axis in Image Container.</param>
        /// <param name="value">Intensity pixel array to set to each layer.</param>
        /// <exception cref="ArgumentException">Thrown when the number of values in array is not valid for the image type.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the X or Y coordinates are out of the image boundaries.</exception>
        /// /// <exception cref="OverflowException">The exception that is thrown when the value exceeds the limit of data type.</exception>
        void SetPixel(int row, int col, byte[] value);
        void SetPixel(int row, int col, ushort[] value);
        void SetPixel(int row, int col, int[] value);
        void SetPixel(int row, int col, float[] value);
        void SetPixel(int row, int col, double[] value);


        /// <summary>
        /// Returns de a PixelVal object with the pixel values in the given coordinates.
        /// </summary>
        /// <remarks>
        /// <para>If the coordinates are out of the matrix, pixel values will be taken according to the BorderMode.</para>
        /// <para>Because of PixelVal we can specify a different border color for each layer in case of BorderMode.Constant.</para>
        /// <para>Values in constantValue will be set as a constant value to each image layer border if they match in number to the image type.</para>
        /// <para>If only one value in constantValue, that value will be set to all layers. If any of these options it throws an exception</para>
        /// <para>When the border type (BorderMode) is Constant, and constantValue is null this will be taken as 0 for all layers.</para>
        /// </remarks>
        /// <param name="row">Position with vertical axis in Image Container.</param>
        /// <param name="col">Position with horizontal axis in Image Container.</param>
        /// <param name="borderMode">Type of border if the coordinate is out of the matrix.</param>
        /// <param name="constantValue">Value used when BorderMode.ConstantValue is selected.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the coordinates are out of the matrix boundaries and the BorderMode is set to ThrowError.</exception>
        /// <returns>The PixelVal object of a specific pixel.</returns>
        PixelVal GetPixel(int row, int col, BorderModes borderMode = BorderModes.ThrowError, PixelVal constantValue = null);

        /// <summary>
        /// Returns the value of the pixel in the given coordinates
        /// </summary>
        /// <remarks>
        /// <para>Obtain the PixeVal object of a specific pixel.</para>
        /// <para>The operation can be implemented with diferent data types.</para>
        /// <para>If the coordinates are out of the matrix, pixel values will be taken according to the BorderMode.</para>
        /// </remarks>
        /// <param name="row">Position with vertical axis in Image Container.</param>
        /// <param name="col">Position with horizontal axis in Image Container.</param>
        /// <param name="channel">Specified a determined layer.</param>
        /// <param name="borderMode">Type of border if the coordinate is out of the matrix.</param>
        /// <param name="constantValue">Value used when BorderMode.ConstantValue is selected.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the coordinates are out of the matrix boundaries and the BorderMode is set to ThrowError.</exception>
        /// <returns>The value of a specific pixel according to the image type.</returns>
        byte GetPixelU8(int row, int col, int channel = 0, BorderModes borderMode = BorderModes.ThrowError, byte constantValue = 0);
        byte[] GetPixelU8Vec(int row, int col, BorderModes borderMode = BorderModes.ThrowError, byte constantValue = 0);
        ushort GetPixelU16(int row, int col, int channel = 0, BorderModes borderMode = BorderModes.ThrowError, ushort constantValue = 0);
        ushort[] GetPixelU16Vec(int row, int col, BorderModes borderMode = BorderModes.ThrowError, ushort constantValue = 0);
        int GetPixelI32(int row, int col, int channel = 0, BorderModes borderMode = BorderModes.ThrowError, int constantValue = 0);
        int[] GetPixelI32Vec(int row, int col, BorderModes borderMode = BorderModes.ThrowError, int constantValue = 0);
        float GetPixelF32(int row, int col, int channel = 0, BorderModes borderMode = BorderModes.ThrowError, float constantValue = 0);
        float[] GetPixelF32Vec(int row, int col, BorderModes borderMode = BorderModes.ThrowError, float constantValue = 0);
        double GetPixelF64(int row, int col, int channel = 0, BorderModes borderMode = BorderModes.ThrowError, double constantValue = 0);
        double[] GetPixelF64Vec(int row, int col, BorderModes borderMode = BorderModes.ThrowError, double constantValue = 0);

        /// <summary>
        /// Returns a new image object that points to a region data of the original image.
        /// </summary>
        /// <remarks>
        /// <para>It only creates a new header for a existing region data.</para>
        /// </remarks>
        /// <param name="rectROI">Rectangle that represents the region of interest.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the Rectangle is out of the matrix boundaries.</exception>
        /// <returns>
        ///  A new image object from a region of interest of an another image object.
        /// </returns>
        IImageContainer SubImage(Rectangle rectROI);

        /// <summary>
        /// Returns a new image object that points to a region data of the original image.
        /// </summary>
        /// <remarks>
        /// <para>It only creates a new header for a existing region data.</para>
        /// </remarks>
        /// <param name="rectROI">Rectangle that represents the region of interest.</param>
        /// <param name="borderMode">Type of border if the coordinate is out of the matrix.</param>
        /// <param name="constantValue">Value used when BorderMode.ConstantValue is selected.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the Rectangle is out of the matrix boundaries.</exception>
        /// <returns>
        ///  A new image object from a region of interest of an another image object.
        /// </returns>
        IImageContainer SubImage(Rectangle rectROI, BorderModes borderMode, double constantValue = 0);

        /// <summary>
        /// Returns a new image object that points to a region data of the original image.
        /// </summary>
        /// <remarks>
        /// <para>It only creates a new header for a existing region data.</para>
        /// </remarks>
        /// <param name="row1">ROI start Y coordinate.</param>
        /// <param name="col1">ROI start X coordinate.</param>
        /// <param name="row2">ROI end Y coordinate.</param>
        /// <param name="col2">ROI end X coordinate.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the x1 of the Rectangle is out of the matrix boundaries.</exception>
        /// <returns>
        ///  A new image object from a region of interest of an another image object.
        /// </returns>
        IImageContainer SubImage(int row1, int col1, int row2, int col2);

        /// <summary>
        /// Returns a region of interest using start and end coordinates.
        /// </summary>
        /// <remarks>
        /// <para>Create a new image from a region of another image Container.</para>
        /// <para>It allocates new data instead of only a new header.</para>
        /// </remarks>
        /// <param name="rectROI">Rectangle that represents the region of interest.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the Rectangle is out of the matrix boundaries.</exception>
        /// <returns>
        /// A new image object from a region of interest saving all the information to new memory addresses.
        /// </returns>
        IImageContainer SubImageClone(Rectangle rectROI);

        /// <summary>
        /// Returns a region of interest using start and end coordinates.
        /// </summary>
        /// <remarks>
        /// <para>Create a new image from a region of another image Container.</para>
        /// <para>It allocates new data instead of only a new header.</para>
        /// </remarks>
        /// <param name="rectROI">Rectangle that represents the region of interest.</param>
        /// <param name="borderMode">Type of border if the coordinate is out of the matrix</param>
        /// <param name="constantValue">Value used when BorderMode.ConstantValue is selected.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the Rectangle is out of the matrix boundaries.</exception>
        /// <returns>
        /// A new image object from a region of interest saving all the information to new memory addresses.
        /// </returns>
        IImageContainer SubImageClone(Rectangle rectROI, BorderModes borderMode, double constantValue = 0);

        /// <summary>
        ///  Returns a region of interest using start and end coordinates.
        /// </summary>
        /// <remarks>
        /// <para>Create a new image from a region of another image Container.</para>
        /// <para>It allocates new data instead of only a new header.</para>
        /// </remarks>
        /// <param name="row1">ROI start Y coordinate.</param>
        /// <param name="col1">ROI start X coordinate.</param>
        /// <param name="row2">ROI end Y coordinate.</param>
        /// <param name="col2">ROI end X coordinate.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the x1 of the Rectangle is out of the matrix boundaries.</exception>
        /// <returns>
        /// A new image object from a region of interest saving all the information to new memory addresses.
        /// </returns>
        IImageContainer SubImageClone(int row1, int col1, int row2, int col2);

        /// <summary>
        ///  Returns an array of Image objects containing each channel separatly.
        /// </summary>
        /// <exception cref="ArgumentNullException">Thrown when the image is null or empty.</exception>
        /// <returns>
        ///  An array that contains image objects.
        /// </returns>
        IImageContainer[] Split();

        /// <summary>
        ///  Converts the image to the new given pixel type.
        /// </summary>
        /// <remarks>
        /// <para>Change the image type of an image object.</para>
        /// <para>Only change the image depth and the layer number is the same.</para>
        /// <para>Values outside of the destination range are truncated to type min and max</para>
        /// </remarks>
        /// <param name="newPixelType">New image type.</param>
        /// <param name="scaleFactor">Factor to multiply pixel values.</param>
        /// <param name="constant">Constant value to be added to pixel value.</param>
        /// <exception cref="ArgumentNullException">Thrown when the image container object is null.</exception>
        void ConvertToType(PixelTypes newPixelType, double scaleFactor = 1, double constant = 0);

        /// <summary>
        /// Creates a new image object with the specified initial parameters.
        /// </summary>
        /// <remarks>
        /// <para>Allocates new data. All the image layers will be set to the value passed as last parameter.</para>
        /// </remarks>
        /// <param name="size">Determine the width and height of an image.</param>
        /// <param name="imageType">Set the image type.</param>
        /// <param name="value">Set the value for all image array.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the value is greater than pixel maximum value.</exception>
        void CreateImage(Size size, ImageTypes imageType, byte value);
        void CreateImage(Size size, ImageTypes imageType, ushort value);
        void CreateImage(Size size, ImageTypes imageType, int value);
        void CreateImage(Size size, ImageTypes imageType, float value);
        void CreateImage(Size size, ImageTypes imageType, double value);

        /// <summary>
        /// Creates a new image object with the specified initial parameters. Allocates new data.
        /// </summary>
        /// <remarks>
        /// <para>Values in PixelVal will be set to each image layer if they match in number.</para>
        /// <para>If only one value in PixelVal, that value will be set to all layers. If any of these options it throws an exception</para>
        /// </remarks>
        /// <param name="size">Determine the width and height of an image.</param>
        /// <param name="imageType">Set the image type.</param>
        /// <param name="value">Pixel value object to set the value for all image array.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the value is greater than pixel maximum value.</exception>
        /// <exception cref="ArgumentException">Thrown when the number of values in PixelVal is not valid for the image type.</exception>
        void CreateImage(Size size, ImageTypes imageType, PixelVal value);

        /// <summary>
        /// Creates a new image object with the specified initial parameters.
        /// </summary>
        /// <remarks>
        /// <para>Allocates new data. All the image layers will be set to the value passed as last parameter.</para>
        /// </remarks>
        /// <param name="sizeX">Determine the width image.</param>
        /// <param name="sizeY">Determine the height image.</param>
        /// <param name="imageType">Set the image type.</param>
        /// <param name="value">Set the value for all image array.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the value is greater than pixel maximum value.</exception>
        void CreateImage(int sizeX, int sizeY, ImageTypes imageType, byte value);
        void CreateImage(int sizeX, int sizeY, ImageTypes imageType, ushort value);
        void CreateImage(int sizeX, int sizeY, ImageTypes imageType, int value);
        void CreateImage(int sizeX, int sizeY, ImageTypes imageType, float value);
        void CreateImage(int sizeX, int sizeY, ImageTypes imageType, double value);

        /// <summary>
        /// Creates a new image object with the specified initial parameters. Allocates new data.
        /// </summary>
        /// <remarks>
        /// <para>Values in PixelVal will be set to each image layer if they match in number.</para>
        /// <para>If only one value in PixelVal, that value will be set to all layers. If any of these options it throws an exception</para>
        /// </remarks>
        /// <param name="sizeX">Determine the width image.</param>
        /// <param name="sizeY">Determine the height image.</param>
        /// <param name="imageType">Set the image type.</param>
        /// <param name="value">Pixel value object to set the value for all image array.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the value is greater than pixel maximum value.</exception>
        /// <exception cref="ArgumentException">Thrown when the number of values in PixelVal is not valid for the image type.</exception>
        void CreateImage(int sizeX, int sizeY, ImageTypes imageType, PixelVal value);

        /// <summary>
        /// Creates a new image object with the specified initial parameters. Allocates new data.
        /// </summary>
        /// <remarks>
        /// <para>Values in PixelVal will be set to each image layer if they match in number.</para>
        /// <para>If only one value in PixelVal, that value will be set to all layers. If any of these options it throws an exception</para>
        /// </remarks>
        /// <param name="size">Determine the width and height of an image.</param>
        /// <param name="pixelType">Set the pixel type from a pixel type object.</param>
        /// <param name="noOfChannels">Set the number of layers.</param>
        /// <param name="value">Set the value for all image array.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the value is greater than pixel maximum value.</exception>
        /// <exception cref="ArgumentException">Thrown when the number of values in PixelVal is not valid for the image type.</exception>
        void CreateImage(Size size, PixelTypes pixelType, int noOfChannels, byte value);
        void CreateImage(Size size, PixelTypes pixelType, int noOfChannels, ushort value);
        void CreateImage(Size size, PixelTypes pixelType, int noOfChannels, int value);
        void CreateImage(Size size, PixelTypes pixelType, int noOfChannels,float value);
        void CreateImage(Size size, PixelTypes pixelType, int noOfChannels, double value);

        /// <summary>
        /// Creates a new image object with the specified initial parameters. Allocates new data.
        /// </summary>
        /// <remarks>
        /// <para>Values in PixelVal will be set to each image layer if they match in number.</para>
        /// <para>If only one value in PixelVal, that value will be set to all layers. If any of these options it throws an exception</para>
        /// </remarks>
        /// <param name="size">Determine the width and height of an image.</param>
        /// <param name="pixelType">Set the pixel type from a pixel type object.</param>
        /// <param name="noOfChannels">Set the number of layers.</param>
        /// <param name="value">Pixel value object to set the value for all image array.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the value is greater than pixel maximum value.</exception>
        /// <exception cref="ArgumentException">Thrown when the number of values in PixelVal is not valid for the image type.</exception>
        void CreateImage(Size size, PixelTypes pixelType, int noOfChannels, PixelVal value);

        /// <summary>
        /// Creates a new image object with the specified initial parameters. Allocates new data.
        /// </summary>
        /// <remarks>
        /// <para>Values in PixelVal will be set to each image layer if they match in number.</para>
        /// <para>If only one value in PixelVal, that value will be set to all layers. If any of these options it throws an exception</para>
        /// </remarks>
        /// <param name="sizeX">Determine the width image.</param>
        /// <param name="sizeY">Determine the height image.</param>
        /// <param name="pixelType">Set the pixel type from a pixel type object.</param>
        /// <param name="noOfChannels">Set the number of layers.</param>
        /// <param name="value">Set the value for all image array.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the value is greater than pixel maximum value.</exception>
        /// <exception cref="ArgumentException">Thrown when the number of values in PixelVal is not valid for the image type.</exception>
        void CreateImage(int sizeX, int sizeY, PixelTypes pixelType, int noOfChannels, byte value);
        void CreateImage(int sizeX, int sizeY, PixelTypes pixelType, int noOfChannels, ushort value);
        void CreateImage(int sizeX, int sizeY, PixelTypes pixelType, int noOfChannels, int value);
        void CreateImage(int sizeX, int sizeY, PixelTypes pixelType, int noOfChannels, float value);
        void CreateImage(int sizeX, int sizeY, PixelTypes pixelType, int noOfChannels, double value);

        /// <summary>
        /// Creates a new image object with the specified initial parameters. Allocates new data.
        /// </summary>
        /// <remarks>
        /// <para>Values in PixelVal will be set to each image layer if they match in number.</para>
        /// <para>If only one value in PixelVal, that value will be set to all layers. If any of these options it throws an exception</para>
        /// </remarks>
        /// <param name="sizeX">Determine the width image.</param>
        /// <param name="sizeY">Determine the height image.</param>
        /// <param name="pixelType">Set the pixel type from a pixel type object.</param>
        /// <param name="noOfChannels">Set the number of layers.</param>
        /// <param name="value">Pixel value object to set the value for all image array.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the value is greater than pixel maximum value.</exception>
        /// <exception cref="ArgumentException">Thrown when the number of values in PixelVal is not valid for the image type.</exception>
        void CreateImage(int sizeX, int sizeY, PixelTypes pixelType, int noOfChannels, PixelVal value);

        /// <summary>
        /// Creates a image object from an existing Bitmap object.
        /// </summary>
        /// <remarks>
        /// <para>It does not allocate new data, it only points to original data.</para>
        /// </remarks>
        /// <param name="bitmapImage">Determine the width image.</param>
        /// <exception cref="ArgumentNullException">Thrown when the Bitmap is null or does not contain any data.</exception>
        void CreateImage(Bitmap bitmapImage);

        /// <summary>
        /// Creates an image object from an existing array buffer.
        /// </summary>
        /// <remarks>
        /// This image uses an array buffer already allocated.
        /// </remarks>
        /// <param name="sizeX">Width of the image.</param>
        /// <param name="sizeY">Heigth of the image.</param>
        /// <param name="pixelType">Pixel format.</param>
        /// <param name="noOfChannels">Number of image layers.</param>
        /// <param name="byteBuffer">An existing array buffer.</param>
        /// <param name="steps">Number of bytes each row occupies. The value should include the padding bytes at the end of the row. Otherwise, steps is calculated as Width * pixelSize.</param>
        void CreateImage(int sizeX, int sizeY, PixelTypes pixelType, int noOfChannels, byte[] byteBuffer, long steps = 0);
        void CreateImage(int sizeX, int sizeY, PixelTypes pixelType, int noOfChannels, ushort[] ushortBuffer, long steps = 0);
        void CreateImage(int sizeX, int sizeY, ImageTypes imageType, byte[] byteBuffer, long steps = 0);
        void CreateImage(int sizeX, int sizeY, ImageTypes imageType, ushort[] ushortBuffer, long steps = 0);
        void CreateImage(Size size, PixelTypes pixelType, int noOfChannels, byte[] byteBuffer, long steps = 0);
        void CreateImage(Size size, PixelTypes pixelType, int noOfChannels, ushort[] ushortBuffer, long steps = 0);
        void CreateImage(Size size, ImageTypes imageType, byte[] byteBuffer, long steps = 0);
        void CreateImage(Size size, ImageTypes imageType, ushort[] ushortBuffer, long steps = 0);

        /// <summary>
        /// Creates an image object from an existing array buffer.
        /// </summary>
        /// <remarks>
        /// This image takes an array buffer and clones it into a new internal array buffer. 
        /// </remarks>
        /// <param name="sizeX">Width of the image.</param>
        /// <param name="sizeY">Heigth of the image.</param>
        /// <param name="pixelType">Pixel format.</param>
        /// <param name="noOfChannels">Number of image layers.</param>
        /// <param name="byteBuffer">An existing array buffer.</param>
        /// <param name="steps">Number of bytes each row occupies. The value should include the padding bytes at the end of the row. Otherwise, steps is calculated as Width * pixelSize.</param>
        void CreateImageCloneBuffer(int sizeX, int sizeY, PixelTypes pixelType, int noOfChannels, byte[] byteBuffer, long steps = 0);
        void CreateImageCloneBuffer(int sizeX, int sizeY, PixelTypes pixelType, int noOfChannels, ushort[] ushortBuffer, long steps = 0);
        void CreateImageCloneBuffer(int sizeX, int sizeY, ImageTypes imageType, byte[] byteBuffer, long steps = 0);
        void CreateImageCloneBuffer(int sizeX, int sizeY, ImageTypes imageType, ushort[] ushortBuffer, long steps = 0);
        void CreateImageCloneBuffer(Size size, PixelTypes pixelType, int noOfChannels, byte[] byteBuffer, long steps = 0);
        void CreateImageCloneBuffer(Size size, PixelTypes pixelType, int noOfChannels, ushort[] ushortBuffer, long steps = 0);
        void CreateImageCloneBuffer(Size size, ImageTypes imageType, byte[] byteBuffer, long steps = 0);
        void CreateImageCloneBuffer(Size size, ImageTypes imageType, ushort[] ushortBuffer, long steps = 0);

        /// <summary>
        /// Returns a new image object that points to the layer data of the original image.
        /// </summary>
        /// <remarks>
        /// <para>It only creates a new header for existing data.</para>
        /// </remarks>
        /// <param name="channel">Channel enum that represents the image layer</param>
        /// <exception cref="ArgumentNullException">Thrown when the image is null or empty.</exception>
        /// <returns>
        /// An image object with a specific image layer.
        /// </returns>
        IImageContainer GetImageChannel(Channels channel);

        /// <summary>
        /// Returns a new image object that points to the layer data of the original image.
        /// </summary>
        /// <remarks>
        /// <para>It only creates a new header for existing data.</para>
        /// </remarks>
        /// <param name="channel">Index that represents the image layer to extract.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the value is greater than the total number of image layers.</exception>
        /// <exception cref="ArgumentNullException">Thrown when the image is null or empty.</exception>
        /// <returns>
        /// An image object with a specific image layer.
        /// </returns>
        IImageContainer GetImageChannel(int channel);

        /// <summary>
        /// Returns a new image object with the data of the specified channel or layer.
        /// </summary>
        /// <remarks>
        /// <para>It copies the whole layer's data to new memory addresses.</para>
        /// </remarks>
        /// <param name="channel">Index that represents the image layer to extract.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the value is greater than the total number of image layers.</exception>
        /// <exception cref="ArgumentNullException">Thrown when the image is null or empty.</exception>
        /// <returns>
        /// An image object with a specific layer.
        /// </returns>
        IImageContainer GetImageChannelClone(int channel);

        /// <summary>
        /// Returns a new image object with the data of the specified channel or layer
        /// </summary>
        /// <remarks>
        /// <para>It copies the whole layer's data to new memory addresses.</para>
        /// </remarks>
        /// <param name="channel">Channel enum that represents the image layer to extract.</param>
        /// <exception cref="ArgumentNullException">Thrown when the image is null or empty.</exception>
        /// <returns>
        /// An image object with a specific layer.
        /// </returns>
        IImageContainer GetImageChannelClone(Channels channel);


        /// <summary>
        /// Returns a Bitmap object from an IImageContainer object.
        /// </summary>
        /// <remarks>
        /// <para>It converts the IImageContainer to a Bitmap object that can be displayed in standard NET controls.</para>
        /// <para>Bitmap can only hold 1, 3 or 4 layers: (Grayscale, RGB and ARGB).</para>
        /// </remarks>
        /// <exception cref="ArgumentNullException">Thrown when the image is null or empty.</exception>
        /// <exception cref="ArgumentException">Thrown when the image type or pixel  type/format is not supported by a Bitmap object.</exception>
        /// <returns>
        /// An bitmap object.
        /// </returns>
        Bitmap GetBitmap();

        #endregion
    }
}
