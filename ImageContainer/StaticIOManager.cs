﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageContainer
{
    /// <summary>
    /// Static Class of the input/output image functions 
    /// </summary>
    public static class StaticIOManager
    {
        /// <summary>
        /// Reads a complete image from a file path
        /// </summary>
        /// <remarks>
        /// The static method works through an instance of the interface it implements and
        /// It can operates over the image object itself setting its properties and pixel data.
        /// </remarks>
        /// <param name="imageInstance">Image object of any class that implements the IImageContainer interface</param>
        /// <param name="url">Path of the image file</param>
        /// <param name="imageMode">Read image mode</param>
        /// <exception cref="System.IO.FileNotFoundException">Thrown when a file does not exist in the specified path.</exception>
        /// <exception cref="ImageFormatNotSupportedException">Thrown when the image format is not supported.</exception>
        public static void ReadImage(IImageContainer imageInstance, string url, ImageModes imageMode = ImageModes.Original)
        {
            imageInstance.ReadImage(url, imageMode);
        }

        /// <summary>
        /// Read a specific image region (with a Rectangle) from a file path.
        /// </summary>
        /// <remarks>
        /// The static method works through an instance of the interface it implements and
        /// It operates over the image object itself setting its properties and pixel data.
        /// </remarks>
        /// <param name="imageInstance">Image object of any class that implements the IImageContainer interface</param>
        /// <param name="url">Path of the image file.</param>
        /// <param name="rectROI">Rectangle that represents the region of interest.</param>
        /// <param name="imageMode">Read image mode.</param>
        /// <exception cref="System.IO.FileNotFoundException">Thrown when a file does not exist in the specified path.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">Throw when the ROI is out of the matrix boundaries</exception>
        /// <exception cref="ImageFormatNotSupportedException">Thrown when the image format is not supported.</exception>
        public static void ReadImage(IImageContainer imageInstance, string url, Rectangle rectROI, ImageModes imageMode = ImageModes.Original)
        {
            imageInstance.ReadImage(url, rectROI, imageMode);
        }

        /// <summary>
        /// Read a specific image region (with a start and end coordinates) from a file path
        /// </summary>
        /// <remarks>
        /// The static method works through an instance of the interface it implements and
        /// It can operates over the image object itself setting its properties and pixel data.
        /// </remarks>
        /// <param name="imageInstance">Image object of any class that implements the IImageContainer interface</param>
        /// <param name="url">Path of the image file</param>
        /// <param name="x1">ROI start X coordinate</param>
        /// <param name="y1">ROI start Y coordinate</param>
        /// <param name="x2">ROI end X coordinate</param>
        /// <param name="y2">ROI end Y coordinate</param>
        /// <param name="imageMode">Read image mode</param>
        /// <exception cref="System.IO.FileNotFoundException">Thrown when a file does not exist in the specified path.</exception>
        /// <exception cref="ImageFormatNotSupportedException">Thrown when the image format is not supported.</exception>
        public static void ReadImage(IImageContainer imageInstance, string url, int x1, int y1, int x2, int y2, ImageModes imageMode = ImageModes.Original)
        {
            imageInstance.ReadImage(url, x1, y1, x2, y2, imageMode);
        }

        /// <summary>
        /// Read a specific image region (with a Rectangle) from a file path
        /// </summary>
        /// <remarks>
        /// <para>
        /// The static method works through an instance of the interface it implements and
        /// It can operates over the image object itself setting its properties and pixel data.
        /// </para>
        /// <para>If the coordinates are out of the matrix, pixel values will be taken according to the BorderMode</para>
        /// </remarks>
        /// <param name="imageInstance">Image object of any class that implements the IImageContainer interface</param>
        /// <param name="url">Path of the image file</param>
        /// <param name="rectROI">Rectangle that represents the region of interest</param>
        /// <param name="imageMode">Read image mode</param>
        /// <param name="borderMode">Type of border if the Rectangle is out of the matrix.</param>
        /// <param name="constantValue">Value used when BorderMode.ConstantValue is selected.</param>
        /// <exception cref="System.IO.FileNotFoundException">Thrown when a file does not exist in the specified path.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">Throw when the ROI is out of the matrix boundaries and the BorderMode is set to ThrowError.</exception>
        /// <exception cref="System.ArgumentNullException">Thrown when the BorderMode.ConstantValue is selected but ConstantValue is null.</exception>
        /// <exception cref="ImageFormatNotSupportedException">Thrown when the image format is not supported.</exception>
        public static void ReadImage(IImageContainer imageInstance, string url, Rectangle rectROI, BorderModes borderMode, double constantValue = 0, ImageModes imageMode = ImageModes.Original)
        {
            imageInstance.ReadImage(url, rectROI, borderMode, constantValue, imageMode);
        }

        /// <summary>
        /// Read a specific image region (with a Rectangle) from a file path
        /// </summary>
        /// <remarks>
        /// <para>
        /// The static method works through an instance of the interface it implements and
        /// It can operates over the image object itself setting its properties and pixel data.
        /// </para>
        /// <para>If the coordinates are out of the matrix, pixel values will be taken according to the BorderMode.</para>
        /// </remarks>
        /// <param name="imageInstance">Image object of any class that implements the IImageContainer interface.</param>
        /// <param name="url">Path of the image file</param>
        /// <param name="x1">ROI start X coordinate</param>
        /// <param name="y1">ROI start Y coordinate</param>
        /// <param name="x2">ROI end X coordinate</param>
        /// <param name="y2">ROI end Y coordinate</param>
        /// <param name="imageMode">Read image mode</param>
        /// <param name="borderMode">Type of border if the Rectangle is out of the matrix.</param>
        /// <param name="constantValue">Value used when BorderMode.ConstantValue is selected.</param>
        /// <exception cref="System.IO.FileNotFoundException">Thrown when a file does not exist in the specified path.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">Throw when the ROI is out of the matrix boundaries and the BorderMode is set to ThrowError.</exception>
        /// <exception cref="System.ArgumentNullException">Thrown when the BorderMode.ConstantValue is selected but ConstantValue is null.</exception>
        /// <exception cref="ImageFormatNotSupportedException">Thrown when the image format is not supported.</exception>
        public static void ReadImage(IImageContainer imageInstance, string url, int x1, int y1, int x2, int y2, BorderModes borderMode, double constantValue = 0, ImageModes imageMode = ImageModes.Original)
        {
            imageInstance.ReadImage(url, x1, y1, x2, y2, borderMode, constantValue, imageMode);
        }

        /// <summary>
        /// Save an image to file path
        /// </summary>
        /// <para>The static method works through an instance of the interface it implements and
        /// It can operates over the image object itself setting its properties and pixel data.</para>
        /// <param name="imageInstance">Image object of any class that implements the IImageContainer interface</param>
        /// <param name="url">Path to save the image</param>
        /// <exception cref="System.ArgumentNullException">Thrown when the image object or the url are null.</exception>
        /// /// <exception cref="System.Runtime.InteropServices.ExternalException">Thrown when the image is saved in the wrong image format or in the same file from which it was created.</exception>
        /// <exception cref="System.Exception">Thrown when the image cannot be saved.</exception>
        public static void SaveImage(IImageContainer imageInstance, string url)
        {
            imageInstance.SaveImage(url);
        }
    }
}
