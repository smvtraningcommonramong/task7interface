﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageContainer
{
    /// <summary>
    /// Channel identifier list 
    /// </summary>
    public enum ImageFileTypes
    {
        Bmp,
        Dib,
        Emf,
        Eps,
        Exif,
        Exp,
        Exr,
        Gif,
        Guid,
        Hdr,
        Hobj,
        Icon,
        Ima,
        Jpg,
        Jpe,
        Jpeg,
        Jpegxr,
        Jp2,
        Jxr,
        Pdf,
        Pgm,
        Pic,
        Png,
        Pnm,
        Ppm,
        Pbm,
        Pxm,
        Psd,
        Ras,
        Sr,
        Svg,
        Svgs,
        Tif,
        Tiff,
        Wmf,
        Webp,
    }
}
