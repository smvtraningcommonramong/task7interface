﻿using System;

namespace ImageContainer
{
    // Pixel class, it holds pixel values.

    public class PixelVal
    {
        private byte[] ValuesU8; // Equal to Byte
        private ushort[] ValuesU16; // Equal to UInt16
        private uint[] ValuesU32; // Equal to UInt32
        private ulong[] ValuesU64; // Equal to UInt64

        private sbyte[] ValuesInt8; // Signed byte. Equal to SByte
        private short[] ValuesInt16; // Equal to Int16
        private int[] ValuesInt32; // Equal to Int32
        private long[] ValuesInt64; // Equal to Int64

        private float[] ValuesF32; 
        private double[] ValuesF64;

        public PixelTypes PixelType { get; private set; }
        public int Length { get; private set; }

        #region Constructors

        #region PixelVal one parameter
        public PixelVal(byte val0)
        {
            this.ValuesU8 = new byte[] { val0 };
            PixelType = PixelTypes.U8;
            this.Length = 1;
        }

        public PixelVal(ushort val0)
        {
            this.ValuesU16 = new ushort[] { val0 };
            PixelType = PixelTypes.U16;
            this.Length = 1;
        }

        public PixelVal(uint val0)
        {
            this.ValuesU32 = new uint[] { val0 };
            PixelType = PixelTypes.U32;
            this.Length = 1;
        }

        public PixelVal(ulong val0)
        {
            this.ValuesU64 = new ulong[] { val0 };
            PixelType = PixelTypes.U64;
            this.Length = 1;
        }

        public PixelVal(sbyte val0)
        {
            this.ValuesInt8 = new sbyte[] { val0 };
            PixelType = PixelTypes.I8;
            this.Length = 1;
        }

        public PixelVal(short val0)
        {
            this.ValuesInt16 = new short[] { val0 }; ;
            PixelType = PixelTypes.I16;
            this.Length = 1;
        }

        public PixelVal(int val0)
        {
            this.ValuesInt32 = new int[] { val0 };
            PixelType = PixelTypes.I32;
            this.Length = 1;
        }

        public PixelVal(long val0)
        {
            this.ValuesInt64 = new long[] { val0 };
            PixelType = PixelTypes.I64;
            this.Length = 1;
        }

        public PixelVal(float val0)
        {
            this.ValuesF32 = new float[] { val0 };
            PixelType = PixelTypes.F32;
            this.Length = 1;
        }

        public PixelVal(double val0)
        {
            this.ValuesF64 = new double[] { val0 };
            PixelType = PixelTypes.F64;
            this.Length = 1;
        }
        #endregion

        #region PixelVal two parameters
        public PixelVal(byte val0, byte val1)
        {
            this.ValuesU8 = new byte[]
            {
                val0,
                val1
            };
            PixelType = PixelTypes.U8;
            this.Length = 2;
        }

        public PixelVal(ushort val0, ushort val1)
        {
            this.ValuesU16 = new ushort[]
            {
                val0,
                val1
            };
            PixelType = PixelTypes.U16;
            this.Length = 2;
        }

        public PixelVal(uint val0, uint val1)
        {
            this.ValuesU32 = new uint[]
            {
                val0,
                val1
            };
            PixelType = PixelTypes.U32;
            this.Length = 2;
        }

        public PixelVal(ulong val0, ulong val1)
        {
            this.ValuesU64 = new ulong[]
            {
                val0,
                val1
            };
            PixelType = PixelTypes.U64;
            this.Length = 2;
        }

        public PixelVal(sbyte val0, sbyte val1)
        {
            this.ValuesInt8 = new sbyte[]
            {
                val0,
                val1
            };
            PixelType = PixelTypes.I8;
            this.Length = 2;
        }

        public PixelVal(short val0, short val1)
        {
            this.ValuesInt16 = new short[]
            {
                val0,
                val1
            };
            PixelType = PixelTypes.I16;
            this.Length = 2;
        }

        public PixelVal(int val0, int val1)
        {
            this.ValuesInt32 = new int[]
            {
                val0,
                val1
            };
            PixelType = PixelTypes.I32;
            this.Length = 2;
        }

        public PixelVal(long val0, long val1)
        {
            this.ValuesInt64 = new long[]
            {
                val0,
                val1
            };
            PixelType = PixelTypes.I64;
            this.Length = 2;
        }

        public PixelVal(float val0, float val1)
        {
            this.ValuesF32 = new float[]
            {
                val0,
                val1
            };
            PixelType = PixelTypes.F32;
            this.Length = 2;
        }

        public PixelVal(double val0, double val1)
        {
            this.ValuesF64 = new double[]
            {
                val0,
                val1
            };
            PixelType = PixelTypes.F64;
            this.Length = 2;
        }
        #endregion

        #region PixelVal three parameters
        public PixelVal(byte val0, byte val1, byte val2)
        {
            this.ValuesU8 = new byte[]
            {
                val0,
                val1,
                val2
            };
            PixelType = PixelTypes.U8;
            this.Length = 3;
        }

        public PixelVal(ushort val0, ushort val1, ushort val2)
        {
            this.ValuesU16 = new ushort[]
            {
                val0,
                val1,
                val2
            };
            PixelType = PixelTypes.U16;
            this.Length = 3;
        }

        public PixelVal(uint val0, uint val1, uint val2)
        {
            this.ValuesU32 = new uint[]
            {
                val0,
                val1,
                val2
            };
            PixelType = PixelTypes.U32;
            this.Length = 3;
        }

        public PixelVal(ulong val0, ulong val1, ulong val2)
        {
            this.ValuesU64 = new ulong[]
            {
                val0,
                val1,
                val2
            };
            PixelType = PixelTypes.U64;
            this.Length = 3;
        }

        public PixelVal(sbyte val0, sbyte val1, sbyte val2)
        {
            this.ValuesInt8 = new sbyte[]
            {
                val0,
                val1,
                val2
            };
            PixelType = PixelTypes.I8;
            this.Length = 3;
        }

        public PixelVal(short val0, short val1, short val2)
        {
            this.ValuesInt16 = new short[]
            {
                val0,
                val1,
                val2
            };
            PixelType = PixelTypes.I16;
            this.Length = 3;
        }

        public PixelVal(int val0, int val1, int val2)
        {
            this.ValuesInt32 = new int[]
            {
                val0,
                val1,
                val2
            };
            PixelType = PixelTypes.I32;
            this.Length = 3;
        }

        public PixelVal(long val0, long val1, long val2)
        {
            this.ValuesInt64 = new long[]
            {
                val0,
                val1,
                val2
            };
            PixelType = PixelTypes.I64;
            this.Length = 3;
        }

        public PixelVal(float val0, float val1, float val2)
        {
            this.ValuesF32 = new float[]
            {
                val0,
                val1,
                val2
            };
            PixelType = PixelTypes.F32;
            this.Length = 3;
        }

        public PixelVal(double val0, double val1, double val2)
        {
            this.ValuesF64 = new double[]
            {
                val0,
                val1,
                val2
            };
            PixelType = PixelTypes.F64;
            this.Length = 3;
        }
        #endregion

        #region PixelVal four parameters
        public PixelVal(byte val0, byte val1, byte val2, byte val3)
        {
            this.ValuesU8 = new byte[]
            {
                val0,
                val1,
                val2,
                val3
            };
            PixelType = PixelTypes.U8;
            this.Length = 4;
        }

        public PixelVal(ushort val0, ushort val1, ushort val2, ushort val3)
        {
            this.ValuesU16 = new ushort[]
            {
                val0,
                val1,
                val2,
                val3
            };
            PixelType = PixelTypes.U16;
            this.Length = 4;
        }

        public PixelVal(uint val0, uint val1, uint val2, uint val3)
        {
            this.ValuesU32 = new uint[]
            {
                val0,
                val1,
                val2,
                val3
            };
            PixelType = PixelTypes.U32;
            this.Length = 4;
        }

        public PixelVal(ulong val0, ulong val1, ulong val2, ulong val3)
        {
            this.ValuesU64 = new ulong[]
            {
                val0,
                val1,
                val2,
                val3
            };
            PixelType = PixelTypes.U64;
            this.Length = 4;
        }

        public PixelVal(sbyte val0, sbyte val1, sbyte val2, sbyte val3)
        {
            this.ValuesInt8 = new sbyte[]
            {
                val0,
                val1,
                val2,
                val3
            };
            PixelType = PixelTypes.I8;
            this.Length = 4;
        }

        public PixelVal(short val0, short val1, short val2, short val3)
        {
            this.ValuesInt16 = new short[]
            {
                val0,
                val1,
                val2,
                val3
            };
            PixelType = PixelTypes.I16;
            this.Length = 4;
        }

        public PixelVal(int val0, int val1, int val2, int val3)
        {
            this.ValuesInt32 = new int[]
            {
                val0,
                val1,
                val2,
                val3
            };
            PixelType = PixelTypes.I32;
            this.Length = 4;
        }

        public PixelVal(long val0, long val1, long val2, long val3)
        {
            this.ValuesInt64 = new long[]
            {
                val0,
                val1,
                val2,
                val3
            };
            PixelType = PixelTypes.I64;
            this.Length = 4;
        }

        public PixelVal(float val0, float val1, float val2, float val3)
        {
            this.ValuesF32 = new float[]
            {
                val0,
                val1,
                val2,
                val3
            };
            PixelType = PixelTypes.F32;
            this.Length = 4;
        }

        public PixelVal(double val0, double val1, double val2, double val3)
        {
            this.ValuesF64 = new double[]
            {
                val0,
                val1,
                val2,
                val3
            };
            PixelType = PixelTypes.F64;
            this.Length = 4;
        }
        #endregion

        #region PixelVal five parameters
        public PixelVal(byte val0, byte val1, byte val2, byte val3, byte val4)
        {
            this.ValuesU8 = new byte[]
            {
                val0,
                val1,
                val2,
                val3,
                val4
            };
            PixelType = PixelTypes.U8;
            this.Length = 5;
        }

        public PixelVal(ushort val0, ushort val1, ushort val2, ushort val3, ushort val4)
        {
            this.ValuesU16 = new ushort[]
            {
                val0,
                val1,
                val2,
                val3,
                val4
            };
            PixelType = PixelTypes.U16;
            this.Length = 5;
        }

        public PixelVal(uint val0, uint val1, uint val2, uint val3, uint val4)
        {
            this.ValuesU32 = new uint[]
            {
                val0,
                val1,
                val2,
                val3,
                val4
            };
            PixelType = PixelTypes.U32;
            this.Length = 5;
        }

        public PixelVal(ulong val0, ulong val1, ulong val2, ulong val3, ulong val4)
        {
            this.ValuesU64 = new ulong[]
            {
                val0,
                val1,
                val2,
                val3,
                val4
            };
            PixelType = PixelTypes.U64;
            this.Length = 5;
        }

        public PixelVal(sbyte val0, sbyte val1, sbyte val2, sbyte val3, sbyte val4)
        {
            this.ValuesInt8 = new sbyte[]
            {
                val0,
                val1,
                val2,
                val3,
                val4
            };
            PixelType = PixelTypes.I8;
            this.Length = 5;
        }

        public PixelVal(short val0, short val1, short val2, short val3, short val4)
        {
            this.ValuesInt16 = new short[]
            {
                val0,
                val1,
                val2,
                val3,
                val4
            };
            PixelType = PixelTypes.I16;
            this.Length = 5;
        }

        public PixelVal(int val0, int val1, int val2, int val3, int val4)
        {
            this.ValuesInt32 = new int[]
            {
                val0,
                val1,
                val2,
                val3,
                val4
            };
            PixelType = PixelTypes.I32;
            this.Length = 5;
        }

        public PixelVal(long val0, long val1, long val2, long val3, long val4)
        {
            this.ValuesInt64 = new long[]
            {
                val0,
                val1,
                val2,
                val3,
                val4
            };
            PixelType = PixelTypes.I64;
            this.Length = 5;
        }

        public PixelVal(float val0, float val1, float val2, float val3, float val4)
        {
            this.ValuesF32 = new float[]
            {
                val0,
                val1,
                val2,
                val3,
                val4
            };
            PixelType = PixelTypes.F32;
            this.Length = 5;
        }

        public PixelVal(double val0, double val1, double val2, double val3, double val4)
        {
            this.ValuesF64 = new double[]
            {
                val0,
                val1,
                val2,
                val3,
                val4
            };
            PixelType = PixelTypes.F64;
            this.Length = 5;
        }
        #endregion

        #region PixelVal Array parameter
        public PixelVal(byte[] values)
        {
            if (values.Length > StaticConstants.SupportedChannels)
            {
                throw new ArgumentOutOfRangeException($"Only {StaticConstants.SupportedChannels} values are accepted.");
            }

            this.ValuesU8 = values;
            PixelType = PixelTypes.U8;
            this.Length = values.Length;
        }

        public PixelVal(ushort[] values)
        {
            if (values.Length > StaticConstants.SupportedChannels)
            {
                throw new ArgumentOutOfRangeException($"Only {StaticConstants.SupportedChannels} values are accepted.");
            }

            this.ValuesU16 = values;
            PixelType = PixelTypes.U16;
            this.Length = values.Length;
        }

        public PixelVal(uint[] values)
        {
            if (values.Length > StaticConstants.SupportedChannels)
            {
                throw new ArgumentOutOfRangeException($"Only {StaticConstants.SupportedChannels} values are accepted.");
            }

            this.ValuesU32 = values;
            PixelType = PixelTypes.U32;
            this.Length = values.Length;
        }

        public PixelVal(ulong[] values)
        {
            if (values.Length > StaticConstants.SupportedChannels)
            {
                throw new ArgumentOutOfRangeException($"Only {StaticConstants.SupportedChannels} values are accepted.");
            }

            this.ValuesU64 = values;
            PixelType = PixelTypes.U64;
            this.Length = values.Length;
        }

        public PixelVal(sbyte[] values)
        {
            if (values.Length > StaticConstants.SupportedChannels)
            {
                throw new ArgumentOutOfRangeException($"Only {StaticConstants.SupportedChannels} values are accepted.");
            }

            this.ValuesInt8 = values;
            PixelType = PixelTypes.I8;
            this.Length = values.Length;
        }

        public PixelVal(short[] values)
        {
            if (values.Length > StaticConstants.SupportedChannels)
            {
                throw new ArgumentOutOfRangeException($"Only {StaticConstants.SupportedChannels} values are accepted.");
            }

            this.ValuesInt16 = values;
            PixelType = PixelTypes.I16;
            this.Length = values.Length;
        }

        public PixelVal(int[] values)
        {
            if (values.Length > StaticConstants.SupportedChannels)
            {
                throw new ArgumentOutOfRangeException($"Only {StaticConstants.SupportedChannels} values are accepted.");
            }

            this.ValuesInt32 = values;
            PixelType = PixelTypes.I32;
            this.Length = values.Length;
        }

        public PixelVal(long[] values)
        {
            if (values.Length > StaticConstants.SupportedChannels)
            {
                throw new ArgumentOutOfRangeException($"Only {StaticConstants.SupportedChannels} values are accepted.");
            }

            this.ValuesInt64 = values;
            PixelType = PixelTypes.I64;
            this.Length = values.Length;
        }

        public PixelVal(float[] values)
        {
            if (values.Length > StaticConstants.SupportedChannels)
            {
                throw new ArgumentOutOfRangeException($"Only {StaticConstants.SupportedChannels} values are accepted.");
            }

            this.ValuesF32 = values;
            PixelType = PixelTypes.F32;
            this.Length = values.Length;
        }

        public PixelVal(double[] values)
        {
            if (values.Length > StaticConstants.SupportedChannels)
            {
                throw new ArgumentOutOfRangeException($"Only {StaticConstants.SupportedChannels} values are accepted.");
            }

            this.ValuesF64 = values;
            PixelType = PixelTypes.F64;
            this.Length = values.Length;
        }
        #endregion

        /// <summary>
        /// Indexer
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public dynamic this[int i]
        {

            get
            {
                switch (this.PixelType)
                {
                    case PixelTypes.U8:
                        return ValuesU8[i];
                    case PixelTypes.U16:
                        return ValuesU16[i];
                    case PixelTypes.U32:
                        return ValuesU32[i];
                    case PixelTypes.U64:
                        return ValuesU64[i];
                    case PixelTypes.I8:
                        return ValuesInt8[i];
                    case PixelTypes.I16:
                        return ValuesInt16[i];
                    case PixelTypes.I32:
                        return ValuesInt32[i];
                    case PixelTypes.I64:
                        return ValuesInt64[i];
                    case PixelTypes.F32:
                        return ValuesF32[i];
                    case PixelTypes.F64:
                        return ValuesF64[i];
                    default:
                        throw new ArgumentException("PixelType not supported.");
                }
            }
            set
            {
                switch (PixelType)
                {
                    case PixelTypes.U8:
                        ValuesU8[i] = value;
                        break;
                    case PixelTypes.U16:
                        ValuesU16[i] = value;
                        break;
                    case PixelTypes.U32:
                        ValuesU32[i] = value;
                        break;
                    case PixelTypes.U64:
                        ValuesU64[i] = value;
                        break;
                    case PixelTypes.I8:
                        ValuesInt8[i] = value;
                        break;
                    case PixelTypes.I16:
                        ValuesInt16[i] = value;
                        break;
                    case PixelTypes.I32:
                        ValuesInt32[i] = value;
                        break;
                    case PixelTypes.I64:
                        ValuesInt64[i] = value;
                        break;
                    case PixelTypes.F32:
                        ValuesF32[i] = value;
                        break;
                    case PixelTypes.F64:
                        ValuesF64[i] = value;
                        break;
                    default:
                        throw new ArgumentException("PixelType not supported.");
                }
            }
        }

        public dynamic GetValues()
        {
            switch (this.PixelType)
            {
                case PixelTypes.U8:
                    return ValuesU8;
                case PixelTypes.U16:
                    return ValuesU16;
                case PixelTypes.U32:
                    return ValuesU32;
                case PixelTypes.U64:
                    return ValuesU64;
                case PixelTypes.I8:
                    return ValuesInt8;
                case PixelTypes.I16:
                    return ValuesInt16;
                case PixelTypes.I32:
                    return ValuesInt32;
                case PixelTypes.I64:
                    return ValuesInt64;
                case PixelTypes.F32:
                    return ValuesF32;
                case PixelTypes.F64:
                    return ValuesF64;
                default:
                    throw new ArgumentException("PixelType not supported.");
            }
        }

        #endregion

        #region Operators

        #region Addition
        public static PixelVal operator +(PixelVal pixel, byte value)
        {
            for (int i = 0; i < pixel.ValuesU8.Length; i++)
            {
                pixel.ValuesU8[i] += value;
            }

            return pixel;
        }

        public static PixelVal operator +(PixelVal pixel, ushort value)
        {
            for (int i = 0; i < pixel.ValuesU16.Length; i++)
            {
                pixel.ValuesU16[i] += value;
            }

            return pixel;
        }

        public static PixelVal operator +(PixelVal pixel, uint value)
        {
            for (int i = 0; i < pixel.ValuesU32.Length; i++)
            {
                pixel.ValuesU32[i] += value;
            }

            return pixel;
        }

        public static PixelVal operator +(PixelVal pixel, ulong value)
        {
            for (int i = 0; i < pixel.ValuesU64.Length; i++)
            {
                pixel.ValuesU64[i] += value;
            }

            return pixel;
        }

        public static PixelVal operator +(PixelVal pixel, sbyte value)
        {
            for (int i = 0; i < pixel.ValuesInt8.Length; i++)
            {
                pixel.ValuesInt8[i] += value;
            }

            return pixel;
        }

        public static PixelVal operator +(PixelVal pixel, short value)
        {
            for (int i = 0; i < pixel.ValuesInt16.Length; i++)
            {
                pixel.ValuesInt16[i] += value;
            }

            return pixel;
        }

        public static PixelVal operator +(PixelVal pixel, int value)
        {
            for (int i = 0; i < pixel.ValuesInt32.Length; i++)
            {
                pixel.ValuesInt32[i] += value;
            }

            return pixel;
        }

        public static PixelVal operator +(PixelVal pixel, long value)
        {
            for (int i = 0; i < pixel.ValuesInt64.Length; i++)
            {
                pixel.ValuesInt64[i] += value;
            }

            return pixel;
        }

        public static PixelVal operator +(PixelVal pixel, float value)
        {
            for (int i = 0; i < pixel.ValuesF32.Length; i++)
            {
                pixel.ValuesF32[i] += value;
            }

            return pixel;
        }

        public static PixelVal operator +(PixelVal pixel, double value)
        {
            for (int i = 0; i < pixel.ValuesF64.Length; i++)
            {
                pixel.ValuesF64[i] += value;
            }

            return pixel;
        }

        public static PixelVal operator +(PixelVal pixel, PixelVal pixel2)
        {
            return pixel.ExecuteArithmeticOperation(pixel, pixel2, '+');
        }
        #endregion

        #region Subtraction

        public static PixelVal operator -(PixelVal pixel, byte value)
        {
            for (int i = 0; i < pixel.ValuesU8.Length; i++)
            {
                pixel.ValuesU8[i] -= value;
            }

            return pixel;
        }

        public static PixelVal operator -(PixelVal pixel, ushort value)
        {
            for (int i = 0; i < pixel.ValuesU16.Length; i++)
            {
                pixel.ValuesU16[i] -= value;
            }

            return pixel;
        }

        public static PixelVal operator -(PixelVal pixel, uint value)
        {
            for (int i = 0; i < pixel.ValuesU32.Length; i++)
            {
                pixel.ValuesU32[i] -= value;
            }

            return pixel;
        }

        public static PixelVal operator -(PixelVal pixel, ulong value)
        {
            for (int i = 0; i < pixel.ValuesU64.Length; i++)
            {
                pixel.ValuesU64[i] -= value;
            }

            return pixel;
        }

        public static PixelVal operator -(PixelVal pixel, sbyte value)
        {
            for (int i = 0; i < pixel.ValuesInt8.Length; i++)
            {
                pixel.ValuesInt8[i] -= value;
            }

            return pixel;
        }

        public static PixelVal operator -(PixelVal pixel, short value)
        {
            for (int i = 0; i < pixel.ValuesInt16.Length; i++)
            {
                pixel.ValuesInt16[i] -= value;
            }

            return pixel;
        }

        public static PixelVal operator -(PixelVal pixel, int value)
        {
            for (int i = 0; i < pixel.ValuesInt32.Length; i++)
            {
                pixel.ValuesInt32[i] -= value;
            }

            return pixel;
        }

        public static PixelVal operator -(PixelVal pixel, long value)
        {
            for (int i = 0; i < pixel.ValuesInt64.Length; i++)
            {
                pixel.ValuesInt64[i] -= value;
            }

            return pixel;
        }

        public static PixelVal operator -(PixelVal pixel, float value)
        {
            for (int i = 0; i < pixel.ValuesF32.Length; i++)
            {
                pixel.ValuesF32[i] -= value;
            }

            return pixel;
        }

        public static PixelVal operator -(PixelVal pixel, double value)
        {
            for (int i = 0; i < pixel.ValuesF64.Length; i++)
            {
                pixel.ValuesF64[i] -= value;
            }

            return pixel;
        }

        public static PixelVal operator -(PixelVal pixel, PixelVal pixel2)
        {
            return pixel.ExecuteArithmeticOperation(pixel, pixel2, '-');
        }
        #endregion

        #region Multiplication

        public static PixelVal operator *(PixelVal pixel, byte value)
        {
            for (int i = 0; i < pixel.ValuesU8.Length; i++)
            {
                pixel.ValuesU8[i] *= value;
            }

            return pixel;
        }

        public static PixelVal operator *(PixelVal pixel, ushort value)
        {
            for (int i = 0; i < pixel.ValuesU16.Length; i++)
            {
                pixel.ValuesU16[i] *= value;
            }

            return pixel;
        }

        public static PixelVal operator *(PixelVal pixel, uint value)
        {
            for (int i = 0; i < pixel.ValuesU32.Length; i++)
            {
                pixel.ValuesU32[i] *= value;
            }

            return pixel;
        }

        public static PixelVal operator *(PixelVal pixel, ulong value)
        {
            for (int i = 0; i < pixel.ValuesU64.Length; i++)
            {
                pixel.ValuesU64[i] *= value;
            }

            return pixel;
        }

        public static PixelVal operator *(PixelVal pixel, sbyte value)
        {
            for (int i = 0; i < pixel.ValuesInt8.Length; i++)
            {
                pixel.ValuesInt8[i] *= value;
            }

            return pixel;
        }

        public static PixelVal operator *(PixelVal pixel, short value)
        {
            for (int i = 0; i < pixel.ValuesInt16.Length; i++)
            {
                pixel.ValuesInt16[i] *= value;
            }

            return pixel;
        }

        public static PixelVal operator *(PixelVal pixel, int value)
        {
            for (int i = 0; i < pixel.ValuesInt32.Length; i++)
            {
                pixel.ValuesInt32[i] *= value;
            }

            return pixel;
        }

        public static PixelVal operator *(PixelVal pixel, long value)
        {
            for (int i = 0; i < pixel.ValuesInt64.Length; i++)
            {
                pixel.ValuesInt64[i] *= value;
            }

            return pixel;
        }

        public static PixelVal operator *(PixelVal pixel, float value)
        {
            for (int i = 0; i < pixel.ValuesF32.Length; i++)
            {
                pixel.ValuesF32[i] *= value;
            }

            return pixel;
        }

        public static PixelVal operator *(PixelVal pixel, double value)
        {
            for (int i = 0; i < pixel.ValuesF64.Length; i++)
            {
                pixel.ValuesF64[i] *= value;
            }

            return pixel;
        }

        public static PixelVal operator *(PixelVal pixel, PixelVal pixel2)
        {
            return pixel.ExecuteArithmeticOperation(pixel, pixel2, '*');
        }
        #endregion

        #region Division

        public static PixelVal operator /(PixelVal pixel, byte value)
        {
            for (int i = 0; i < pixel.ValuesU8.Length; i++)
            {
                pixel.ValuesU8[i] /= value;
            }

            return pixel;
        }

        public static PixelVal operator /(PixelVal pixel, ushort value)
        {
            for (int i = 0; i < pixel.ValuesU16.Length; i++)
            {
                pixel.ValuesU16[i] /= value;
            }

            return pixel;
        }

        public static PixelVal operator /(PixelVal pixel, uint value)
        {
            for (int i = 0; i < pixel.ValuesU32.Length; i++)
            {
                pixel.ValuesU32[i] /= value;
            }

            return pixel;
        }

        public static PixelVal operator /(PixelVal pixel, ulong value)
        {
            for (int i = 0; i < pixel.ValuesU64.Length; i++)
            {
                pixel.ValuesU64[i] /= value;
            }

            return pixel;
        }

        public static PixelVal operator /(PixelVal pixel, sbyte value)
        {
            for (int i = 0; i < pixel.ValuesInt8.Length; i++)
            {
                pixel.ValuesInt8[i] /= value;
            }

            return pixel;
        }

        public static PixelVal operator /(PixelVal pixel, short value)
        {
            for (int i = 0; i < pixel.ValuesInt16.Length; i++)
            {
                pixel.ValuesInt16[i] /= value;
            }

            return pixel;
        }

        public static PixelVal operator /(PixelVal pixel, int value)
        {
            for (int i = 0; i < pixel.ValuesInt32.Length; i++)
            {
                pixel.ValuesInt32[i] /= value;
            }

            return pixel;
        }

        public static PixelVal operator /(PixelVal pixel, long value)
        {
            for (int i = 0; i < pixel.ValuesInt64.Length; i++)
            {
                pixel.ValuesInt64[i] /= value;
            }

            return pixel;
        }

        public static PixelVal operator /(PixelVal pixel, float value)
        {
            for (int i = 0; i < pixel.ValuesF32.Length; i++)
            {
                pixel.ValuesF32[i] /= value;
            }

            return pixel;
        }

        public static PixelVal operator /(PixelVal pixel, double value)
        {
            for (int i = 0; i < pixel.ValuesF64.Length; i++)
            {
                pixel.ValuesF64[i] /= value;
            }

            return pixel;
        }

        public static PixelVal operator /(PixelVal pixel, PixelVal pixel2)
        {
            return pixel.ExecuteArithmeticOperation(pixel, pixel2, '/');
        }
        #endregion

        #region Comparison
        public static bool operator >(PixelVal pixel, PixelVal pixel2)
        {
            return pixel.ExecuteComparisonOperation(pixel, pixel2, ">");
        }

        public static bool operator <(PixelVal pixel, PixelVal pixel2)
        {
            return pixel.ExecuteComparisonOperation(pixel, pixel2, "<");
        }

        public static bool operator >=(PixelVal pixel, PixelVal pixel2)
        {
            return pixel.ExecuteComparisonOperation(pixel, pixel2, ">=");
        }

        public static bool operator <=(PixelVal pixel, PixelVal pixel2)
        {
            return pixel.ExecuteComparisonOperation(pixel, pixel2, "<=");
        }

        #endregion

        #region Equality
        public static bool operator ==(PixelVal pixel, PixelVal pixel2)
        {
            return pixel.Equals(pixel2);
        }

        public static bool operator !=(PixelVal pixel, PixelVal pixel2)
        {
            return pixel.ExecuteComparisonOperation(pixel, pixel2, "!=");
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            return obj is PixelVal && Equals(obj);
        }

        public bool Equals(PixelVal pixel)
        {
            return pixel.ExecuteComparisonOperation(this, pixel, "==");
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #endregion

        #endregion

        #region Helper functions

        private PixelVal ExecuteArithmeticOperation(PixelVal pixel, PixelVal pixel2, char operation)
        {

            switch (PixelType)
            {
                case PixelTypes.U8:
                    pixel.ValuesU8 = GetNewPixelArray(pixel.ValuesU8, pixel2.ValuesU8, operation);
                    return pixel;
                case PixelTypes.U16:
                    pixel.ValuesU16 = GetNewPixelArray(pixel.ValuesU16, pixel2.ValuesU16, operation);
                    return pixel;
                case PixelTypes.U32:
                    pixel.ValuesU8 = GetNewPixelArray(pixel.ValuesU32, pixel2.ValuesU32, operation);
                    return pixel;
                case PixelTypes.U64:
                    pixel.ValuesU8 = GetNewPixelArray(pixel.ValuesU64, pixel2.ValuesU64, operation);
                    return pixel;
                case PixelTypes.I8:
                    pixel.ValuesU8 = GetNewPixelArray(pixel.ValuesInt8, pixel2.ValuesInt8, operation);
                    return pixel;
                case PixelTypes.I16:
                    pixel.ValuesU8 = GetNewPixelArray(pixel.ValuesInt16, pixel2.ValuesInt16, operation);
                    return pixel;
                case PixelTypes.I32:
                    pixel.ValuesU8 = GetNewPixelArray(pixel.ValuesInt32, pixel2.ValuesInt32, operation);
                    return pixel;
                case PixelTypes.I64:
                    pixel.ValuesU8 = GetNewPixelArray(pixel.ValuesInt64, pixel2.ValuesInt64, operation);
                    return pixel;
                case PixelTypes.F32:
                    pixel.ValuesU8 = GetNewPixelArray(pixel.ValuesF32, pixel2.ValuesF32, operation);
                    return pixel;
                case PixelTypes.F64:
                    pixel.ValuesU8 = GetNewPixelArray(pixel.ValuesF64, pixel2.ValuesF64, operation);
                    return pixel;
                default:
                    throw new ArgumentOutOfRangeException("Pixel type not supported.");
            }
        }

        private dynamic GetNewPixelArray(dynamic valuesPixel1, dynamic valuesPixel2, char opera)
        {

            for (int i = 0; i < valuesPixel1.Length; i++)
            {
                switch (opera)
                {
                    case '+':
                        valuesPixel1[i] += valuesPixel2[i];
                        break;
                    case '-':
                        valuesPixel1[i] -= valuesPixel2[i];
                        break;
                    case '*':
                        valuesPixel1[i] *= valuesPixel2[i];
                        break;
                    case '/':
                        if (valuesPixel2[i] == 0)
                        {
                            throw new DivideByZeroException();
                        }
                        valuesPixel1[i] /= valuesPixel2[i];
                        break;
                    default:
                        break;
                }
            }

            return valuesPixel1;
        }

        private bool ExecuteComparisonOperation(PixelVal pixel, PixelVal pixel2, string operation)
        {
            switch (PixelType)
            {
                case PixelTypes.U8:
                    return ComparisonResult(pixel.ValuesU8, pixel2.ValuesU8, operation);
                case PixelTypes.U16:
                    return ComparisonResult(pixel.ValuesU16, pixel2.ValuesU16, operation);
                case PixelTypes.U32:
                    return ComparisonResult(pixel.ValuesU32, pixel2.ValuesU32, operation);
                case PixelTypes.U64:
                    return ComparisonResult(pixel.ValuesU64, pixel2.ValuesU64, operation);
                case PixelTypes.I8:
                    return ComparisonResult(pixel.ValuesInt8, pixel2.ValuesInt8, operation);
                case PixelTypes.I16:
                    return ComparisonResult(pixel.ValuesInt16, pixel2.ValuesInt16, operation);
                case PixelTypes.I32:
                    return ComparisonResult(pixel.ValuesInt32, pixel2.ValuesInt32, operation);
                case PixelTypes.I64:
                    return ComparisonResult(pixel.ValuesInt64, pixel2.ValuesInt64, operation);
                case PixelTypes.F32:
                    return ComparisonResult(pixel.ValuesF32, pixel2.ValuesF32, operation);
                case PixelTypes.F64:
                    return ComparisonResult(pixel.ValuesF64, pixel2.ValuesF64, operation);
                default:
                    throw new ArgumentOutOfRangeException("Pixel type not supported.");
            }
        }

        private bool ComparisonResult(dynamic valuesPixel1, dynamic valuesPixel2, string opera)
        {
            var passTestCount = 0;

            for (int i = 0; i < valuesPixel1.Length; i++)
            {
                switch (opera)
                {
                    case ">":
                        if (valuesPixel1[i] > valuesPixel2[i])
                        {
                            passTestCount++;
                        }
                        else
                        {
                            return false;
                        }
                        break; 
                    case "<":
                        if (valuesPixel1[i] < valuesPixel2[i])
                        {
                            passTestCount++;
                        }
                        else
                        {
                            return false;
                        }
                        break;
                    case ">=":
                        if (valuesPixel1[i] >= valuesPixel2[i])
                        {
                            passTestCount++;
                        }
                        else
                        {
                            return false;
                        }
                        break;
                    case "<=":
                        if (valuesPixel1[i] <= valuesPixel2[i])
                        {
                            passTestCount++;
                        }
                        else
                        {
                            return false;
                        }
                        break;
                    case "==":
                        if (valuesPixel1[i] == valuesPixel2[i])
                        {
                            passTestCount++;
                        }
                        else
                        {
                            return false;
                        }
                        break;
                    case "!=":
                        if (valuesPixel1[i] != valuesPixel2[i])
                        {
                            return true; 
                        }
                        passTestCount++;
                        break;
                    default:
                        break;
                }
            }

            return passTestCount == valuesPixel1.Length;
        }

        #endregion
    }
}
