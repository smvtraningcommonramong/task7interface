﻿
namespace ImageContainer
{
    /// <summary>
    /// Interpolation Mode for the pixels
    /// </summary>
    public enum InterpolationModes
    {
        Bilinear,
        NearestNeighbor,
        Linear,
        Cubic
    }
}
